module.exports = {
	printWidth: 90,
	singleQuote: true,
	tabWidth: 2,
	useTabs: false,
	semi: false,
	trailingComma: 'none',
	arrowParens: 'avoid',
	bracketSpacing: true,
	jsxSingleQuote: true
}
