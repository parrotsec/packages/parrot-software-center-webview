export default {
  INSTALL: 'queue/INSTALL',
  UNINSTALL: 'queue/UNINSTALL',
  SWAP: 'queue/SWAP',
  DELETE: 'queue/DELETE'
}
