export default {
  SET: 'alert/SET',
  CLEAR: 'alert/CLEAR'
}
