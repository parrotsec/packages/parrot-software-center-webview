export default {
  TOKEN_REQUEST: 'tokens/REQUEST',
  TOKEN_SUCCESS: 'tokens/SUCCESS',
  TOKEN_FAILURE: 'tokens/FAILURE',
  TOKEN_CLEAR: 'tokens/CLEAR'
}
