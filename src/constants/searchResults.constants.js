export default {
  SET_PAGE: 'searchResults/SET_PAGE',
  SET_RESULTS: 'searchResults/SET_RESULTS',
  SET_NAMES: 'searchResults/SET_NAMES'
}
