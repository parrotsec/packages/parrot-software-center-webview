export const delayPromise = ms => new Promise(resolve => setTimeout(resolve, ms))
